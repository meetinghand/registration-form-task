# Registration Form Task

## Form Properties

- The video showing the form stages is in the repo.
- The source json file required for the dynamic creation of the form content and the json file showing the sample output structure are in the repo.
- More than one participant registration can be made.
- There are validation rules.
- For each participant registration, 1 registration type and more than one workshop selection can be made.
- When the form is submitted, it is sufficient to display the payload in the consol.

## Technical Details

- It should be developed with one of the frameworks such as Vue, react, angular. Preferably Vue.
- The relevant framework's state management tool should be used. (Vuex, Redux etc.)
- SASS should be used. Bootstrap, Vuetify etc. usable
- The form can be designed in the desired structure. It should be responsive.


----------------------------------------------------------------------------

## Form Özellikleri

- Form aşamalarının gösterildiği video repo içerisindedir.
- Form içeriğinin dinamik olarak oluşturulması için gereken kaynak json dosyası ve örnek çıktı yapısını gösteren json dosyası repo içerisindedir.
- Birden fazla katılımcı kaydı yapılabilecek.
- Validasyon kuralları bulunmaktadır.
- Her bir katılımcı kaydı için 1 tane kayıt türü, birden fazla workshop seçimi yapılabilecektir.
- Form submit edildiğinde payload'ın consolda görüntülenmesi yeterlidir.

## Gereksinimler

- Vue, react, angular gibi frameworklerden biri ile geliştirilmelidir. Tercihen Vue.
- İlgili frameworkün state management aracı kullanılmalı.
- SASS kullanılmalı. Bootstrap, Vuetify vs. kullanılabilir
- Responsive olması şartıyla form istenilen yapıda tasarlanabilir.